package fr.guillaume.stat;


import fr.guillaume.util.PropertiesParser;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.List;
import java.util.Map;

public class PartieStat {

    private static int passage[] = {0, 0, 0, 0, 0}; //zone1,zone2 zone3,zone4
    private static int zone[] = {0, 0, 0, 0, 0}; //zone1,zone2 zone3,zone4

    public static void main(String[] args) throws IOException, URISyntaxException {

        System.out.println("Partie théorique");

        //Permet de récupérer le fichier qui se situe dans resources et de le copier dans le fichier courant
        InputStream inputStream = PartieStat.class.getClassLoader().getResourceAsStream("config_stat.properties");
        File file = new File(new File(PartieStat.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getParentFile(), "config_stat.properties"); //On remonte dans l'arboressance on obtient donc le chemin du .jar

        //Si le fichier existe pas on copie celui qui se trouve dans resources et on le met dans le fichier ou se trouve le .jar
        if (!file.exists()) {
            System.out.println("Creating " + file.getName());
            assert inputStream != null;
            Files.copy(inputStream, file.toPath());
        }

        //On créé l'objet fichier
        PropertiesParser parser = new PropertiesParser(new FileInputStream(file));

        //On crée une map qui est une sorte de dico comme en python
        Map<String, List<Double>> values = null;
        try {
            values = parser.read();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }

        List<Double> ProA = values.get("pro-a");
        List<Double> ProB = values.get("pro-b");
        List<Double> ProC = values.get("pro-c");
        List<Double> ProD = values.get("pro-d");
        List<Double> ProE = values.get("pro-e");

        //Récupére la zone de départ puis on la met a l'entier le plus proche que l'on cast en int
        int ZoneDepart = (int) Math.round(values.get("zone-depart").get(0));
        zone[ZoneDepart] = 1; //Zone de départ

        int nb_simulation = (int) Math.round(values.get("nb-simulation").get(0));

        for (int i = 0; i < nb_simulation; i++) {


            if (zone[1] == 1) {

                probazone(1, ProB);
                passage[1] = passage[1] + 1;
                continue;
            }

            if (zone[0] == 1) {

                probazone(0, ProA);
                passage[0] = passage[0] + 1;
                continue;
            }

            if (zone[2] == 1) {

                probazone(2, ProC);
                passage[2] = passage[2] + 1;
                continue;
            }

            if (zone[3] == 1) {

                probazone(3, ProD);
                passage[3] = passage[3] + 1;
                continue;
            }

            if (zone[4] == 1) {

                probazone(4, ProE);
                passage[4] = passage[4] + 1;
                continue;
            }

        }


        System.out.println("Résultat :");
        if (passage[0] != 0) {
            affichage("PA", passage[0], nb_simulation);
        }

        if (passage[1] != 0) {
            affichage("PB", passage[1], nb_simulation);
        }

        if (passage[2] != 0) {
            affichage("PC", passage[2], nb_simulation);
        }
        if (passage[3] != 0) {
            affichage("PD", passage[3], nb_simulation);
        }
        if (passage[4] != 0) {
            affichage("PE", passage[4], nb_simulation);
        }


    }

    private static void probazone(int nbzone, List<Double> proba) {

        double rand = Math.random();

        if (rand <= proba.get(0)) {
            zone[nbzone] = 0;
            zone[0] = 1;
        }

        if (rand <= proba.get(0) + proba.get(1) && rand >= proba.get(0)) {
            zone[nbzone] = 0;
            zone[1] = 1;
        }

        if (rand <= proba.get(0) + proba.get(1) + proba.get(2) && rand >= proba.get(0) + proba.get(1)) {
            zone[nbzone] = 0;
            zone[2] = 1;
        }
        if (rand <= proba.get(0) + proba.get(1) + proba.get(2) + proba.get(3) && rand >= proba.get(0) + proba.get(1) + proba.get(2)) {
            zone[nbzone] = 0;
            zone[3] = 1;
        }
        if (rand <= proba.get(0) + proba.get(1) + proba.get(2) + proba.get(3) + proba.get(4) && rand >= proba.get(0) + proba.get(1) + proba.get(2) + proba.get(3)) {
            zone[nbzone] = 0;
            zone[4] = 1;
        }


    }

    private static void affichage(String nom_proba, int nb, int nb_simulation) {
        System.out.println("Nombre de fois que l'on passe dans " + nom_proba + " : " + nb);
        System.out.println("On a donc une proba de : " + (double) nb / nb_simulation + " pour " + nom_proba);
    }

}

