package fr.guillaume.niveauc;


import fr.guillaume.util.PropertiesParser;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.Files;

import java.util.List;
import java.util.Map;


public class NiveauC {

    public static void main(String[] args) throws IOException, URISyntaxException {


        System.out.println("Partie Statistique");


        //Permet de récupérer le fichier qui se situe dans resources et de le copier dans le fichier courant
        InputStream inputStream = NiveauC.class.getClassLoader().getResourceAsStream("config_theorique.properties");
        File folder = new File(NiveauC.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getParentFile();
        File file = new File(folder, "config_theorique.properties");
        // new File(dossier, enfant);
        //Si le fichier existe pas on copie celui qui se trouve dans resources et on le met dans le fichier ou se trouve le .jar

        if (!file.exists()) {
            System.out.println("Creating " + file.getName());
            assert inputStream != null;
            Files.copy(inputStream, file.toPath());
        }

        //On créé l'objet fichier
        PropertiesParser parser = new PropertiesParser(new FileInputStream(file));

        //On crée une map qui est une sorte de dico comme en python
        Map<String, List<Double>> values = null;
        try {
            values = parser.read();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }

        //proba conditionnelle ex: de B sachant A
        List<Double> ProA = values.get("pro-a");
        List<Double> ProB = values.get("pro-b");
        List<Double> ProC = values.get("pro-c");
        List<Double> ProD = values.get("pro-d");
        List<Double> ProE = values.get("pro-e");

        //nb de départ
        double PA = values.get("PA").get(0), Pa2 = PA;
        double PB = values.get("PB").get(0), Pb2 = PB;
        double PC = values.get("PC").get(0), Pc2 = PC;
        double PD = values.get("PD").get(0), Pd2 = PD;
        double PE = values.get("PE").get(0), Pe2 = PE;

        int nb_simulation = (int) Math.round(values.get("nb-simulation").get(0));


        for (int i = 0; i < nb_simulation; i++) {
            PA = ProA.get(0) * Pa2 + ProA.get(1) * Pb2 + ProA.get(2) * Pc2 + ProA.get(3) * Pd2 + ProA.get(4) * Pe2;
            PB = ProB.get(0) * Pa2 + ProB.get(1) * Pb2 + ProB.get(2) * Pc2 + ProB.get(3) * Pd2 + ProB.get(4) * Pe2;
            PC = ProC.get(0) * Pa2 + ProC.get(1) * Pb2 + ProC.get(2) * Pc2 + ProC.get(3) * Pd2 + ProC.get(4) * Pe2;
            PD = ProD.get(0) * Pa2 + ProD.get(1) * Pb2 + ProD.get(2) * Pc2 + ProD.get(3) * Pd2 + ProD.get(4) * Pe2;
            PE = ProE.get(0) * Pa2 + ProE.get(1) * Pb2 + ProE.get(2) * Pc2 + ProE.get(3) * Pd2 + ProE.get(4) * Pe2;
            Pa2 = PA;
            Pb2 = PB;
            Pc2 = PC;
            Pd2 = PD;
            Pe2 = PE;
        }


        if (PA != 0.0) {
            System.out.println("Proba PA " + PA);
        }
        if (PB != 0.0) {
            System.out.println("Proba PB " + PB);
        }
        if (PC != 0.0) {
            System.out.println("Proba Pc " + PC);
        }
        if (PD != 0.0) {
            System.out.println("Proba Pd " + PD);
        }
        if (PE != 0.0) {
            System.out.println("Proba Pe " + PE);
        }

    }

}
