package fr.guillaume.util;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PropertiesParser {

    private InputStream inputStream;

    public PropertiesParser(InputStream inputStream){
        this.inputStream = inputStream;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public Map<String, List<Double>> read() throws IOException {
        Map<String, List<Double>> result = new HashMap<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        String line = reader.readLine();
        while (line != null){
            if(line.startsWith("#")){
                line = reader.readLine();
                continue;
            }
            String[] keyAndValues = line.split("=");
            String[] values = keyAndValues[1].split(",");
            List<Double> doubles = new ArrayList<>();
            for(String rawValue : values){
                doubles.add(Double.parseDouble(rawValue));
            }
            result.put(keyAndValues[0], doubles);
            line = reader.readLine();
        }
        reader.close();
        return result;
    }
}
